//version inicial
//Constantes para Encriptar
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
//Termina para encriptar


var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
//agregado
var movimientosV2JSON = require('./movimientosv2.json');

//consumir desde el body
var bodyparser = require('body-parser');
app.use(bodyparser.json());

//
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

// regresar Json de MLAB
var requestjson = require('request-json');
var urlClientesMLab = 'https://api.mlab.com/api/1/databases/onogales/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var urlUsuariosMLab = 'https://api.mlab.com/api/1/databases/onogales/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';

var clienteMLab = requestjson.createClient(urlClientesMLab);
var usuarioMlab = requestjson.createClient(urlUsuariosMLab);
//////

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/onogales/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;
var usuarioMLabRaiz;
////////

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res) {
  //res.send('Hemos recibido su peticion get'); //solo regresa la cadena
  res.sendFile(path.join(__dirname,'index.html')); //regresa todo un archivo
})


app.post('/',function(req, res){
  res.send('Hemos recibido su peticion Post');
})


app.put('/',function(req, res){
  res.send('Hemos recibido su peticion Put');
})

app.delete('/',function(req, res){
  res.send('Hemos recibido su peticion Delete');
})

app.get('/v1/Clientes/:idcliente',function(req, res){
  res.send('Aqui tiene al cliente numero: ' + req.params.idcliente);
})


app.post('/v1/Clientes/:idcliente',function(req, res){
  res.send('Aqui tiene al cliente numero: ' + req.params.idcliente);
})


app.put('/v1/Clientes/:idcliente',function(req, res){
  res.send('Aqui tiene al cliente numero: ' + req.params.idcliente);
})


app.delete('/v1/Clientes/:idcliente',function(req, res){
  res.send('Aqui tiene al cliente numero: ' + req.params.idcliente);
})

app.get('/v1/movimientos',function(req, res){
  res.sendfile('movimientosv1.json'); //regresa fichero json
})

app.get('/v2/movimientos',function(req, res){
  res.json(movimientosV2JSON); //regresa fichero json
})

app.get('/v2/movimientos/:index',function(req, res){ //regresa el index del elemento JSON
  console.log(req.params.index);
  res.send(movimientosV2JSON[req.params.index - 1]);
})

app.get('/v2/movimientosquery',function(req, res){ //entra query desde url (json)
  console.log(req.query);
  res.send('recibido');
})

app.post('/v2/movimientos',function(req, res){ //Se envia JSON desde body
  var nuevo = req.body
  nuevo.id = movimientosV2JSON.length + 1
  movimientosV2JSON.push(nuevo)
  res.send('movimiento dado de alta')
})

app.put('/v2/movimientos',function(req, res){
  res.send('Hemos recibido ....')
})

//obtenemos los clientes de MLAB
app.get('/v3/Clientes',function(req,res){
  clienteMLab.get('',function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

//Insertamos clientes de MLAB
app.post('/v3/Clientes', function(req, res){
  clienteMLab.post('',req.body, function(err, resM, body){
    res.send(body);
  })
})

////////////////////////////////////////////////////////////////////////////////////////////
//Insert de usuario
app.post('/v4/AltaUsuario', function(req, res){
  password = encrypt(req.body.password);
  req.body.password = password;
    usuarioMlab.post('',req.body, function(err, resM, body){
      res.send(body);
    })
  })



  //obtenemos todos los Usuarios
  app.get('/v4/Usuarios',function(req,res){
    usuarioMlab.get('',function(err, resM, body){
      if(err){
        console.log(body);
      }else{
        res.send(body);
      }
    })
  })


    //obtenemos a Cliente especìfico
    app.post('/v4/Clientes',function(req,res){

      var cliente = req.body.id_cliente;
      var query = 'q={"id_cliente":'+ cliente + '}';

      clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Clientes?" + apiKey + "&" + query);
      console.log(urlMLabRaiz + "/Clientes?" + apiKey + "&" + query);

      clienteMLabRaiz.get('',function(err, resM, body){
        if (!err) {
          if (body.length == 1) {
            res.send(body); //Cliente encontrado
          }else{
            res.status(404).send('Cliente NO Encontrado: ' + cliente);
          }
        }else {
          console.log(body);
        }
      })
    })


//insertar movimientos de Clientes (insert)
app.put('/v4/inmovimientos',function(req, res){
  var id_cliente = req.body.id_cliente;
  var query = 'q={"id_cliente":'+ id_cliente + '}';
  //var listado = '{ "listado" : {"fecha":"'+req.body.fecha +'","importe":'+req.body.importe + ',"tipo":"'+ req.body.tipo +'"}}';
  var listado = '{"fecha":"'+req.body.fecha +'","importe":'+req.body.importe + ',"tipo":"'+ req.body.tipo +'"}';
  var listajson = JSON.parse(listado);
  //var jsoncliente = require(urlMLabRaiz + "/Clientes?" + apiKey + "&" + query);


  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Clientes?" + apiKey + "&" + query);
  //console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);

  //clienteMLabRaiz.push('',listajson, function(err, resM, body){
  clienteMLabRaiz.push('',listado, function(err, resM, body){
  //clienteMLabRaiz.put('',listajson, function(err, resM, body){
    res.send(body);
  })

    //clienteMLabRaiz.push('',JSON.parse(listado), function(err, resM, body){
    //clienteMLabRaiz.push(JSON.parse(listado))

    //})


})


// Para el logueo serà Post
app.post('/v4/login', function(req, res){
  var email = req.body.email
  var password = req.body.password

  //funcion que encrypta contraseña
  password = encrypt(password);
  console.log("Password Encriptado : " + password);
  //password = decrypt(password);


  var query = 'q={"email":"'+ email +'","password.iv":"'+ password.iv + '","password.encryptedData":"'+ password.encryptedData + '"}';

  usuarioMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
  console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);

  usuarioMLabRaiz.get('',function(err, resM, body){
    if (!err) {
      if (body.length == 1) { //Login ok
        res.status(200).send('Usuario Logueado ' + password.iv + ' ' + password.encryptedData);
      }else{
        res.status(404).send('Usuario No Logueado ' +  password.iv + ' ' + password.encryptedData);
      }
    }else {
      console.log(body);
    }
  })
})


function encrypt(text) {
 let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
 let encrypted = cipher.update(text);
 encrypted = Buffer.concat([encrypted, cipher.final()]);
 return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

function decrypt(text) {
 let iv = Buffer.from(text.iv, 'hex');
 let encryptedText = Buffer.from(text.encryptedData, 'hex');
 let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
 let decrypted = decipher.update(encryptedText);
 decrypted = Buffer.concat([decrypted, decipher.final()]);
 return decrypted.toString();
}
